import { WebSocket } from 'ws';

const URL =
  'wss://stream.binancefuture.com/ws/REHc6gBrnevUgyFCOiWpL85kcvCJ6iBt0sZjfUHUIeFcQKuybeX5Ko7wYoBLgger';
console.log(URL);

function connect(iterator: number) {
  const ws = new WebSocket(URL);
  ws.onopen = function() {
    // subscribe to some channels
    console.log(iterator, ' connections');
    // ws.send(
    //   JSON.stringify({
    //     method: 'SUBSCRIBE',
    //     params: ['btcusdt@aggTrade', 'btcusdt@depth'],
    //     id: 1,
    //   }),
    // );
  };

  ws.onmessage = function(e) {
    console.log('Message: ', iterator, ' - ', e.data);
  };

  ws.onclose = function(e) {
    console.log(
      'Socket is closed. Reconnect will be attempted in 1 second.',
      e.reason,
    );
    setTimeout(function() {
      connect(iterator);
    }, 1000);
  };

  ws.onerror = function(err) {
    console.error('Socket encountered error: ', err.message, 'Closing socket');
    ws.close();
  };
}
for (let i = 1; i < 201; i++) {
  connect(i);
}
